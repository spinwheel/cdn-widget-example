import React, { useEffect } from 'react';

export const LiabilityBreakdown = ({ openWidget }) => {
  useEffect(() => {
    const fetchTokenAndOpenDim = async () => {
      openWidget('liability-breakdown', 'widget-container-i');
    };
    fetchTokenAndOpenDim();
  }, [openWidget]);

  return (
    <div
      style={{
        width: '90%',
        margin: '1rem 0',
        height: '300px',
        overflow: 'auto',
        borderRadius: '0.5rem',
      }}
      id="widget-container-i"
    />
  );
};
