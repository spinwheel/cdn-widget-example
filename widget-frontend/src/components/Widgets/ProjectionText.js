import React, { useEffect } from 'react';

export const ProjectionText = ({ openWidget }) => {
  useEffect(() => {
    const fetchTokenAndOpenDim = async () => {
      openWidget('projection-text', 'widget-container-ii');
    };
    fetchTokenAndOpenDim();
  }, [openWidget]);

  return (
      <div
        style={{ marginBottom: '1rem', height: '80px', width: '90%', overflow: 'hidden' }}
        id="widget-container-ii"
      />
  );
};
