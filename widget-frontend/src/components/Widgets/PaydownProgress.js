import React, { useEffect } from 'react';

export const PaydownProgress = ({ openWidget }) => {
  useEffect(() => {
    const fetchTokenAndOpenDim = async () => {
      openWidget('paydown-progress', 'widget-container-iii');
    };
    fetchTokenAndOpenDim();
  }, [openWidget]);

  return (
    <div
      style={{ height: '50px', width: '90%', overflow: 'hidden' }}
      id="widget-container-iii"
    />
  );
};
