import React, { useCallback, useEffect, useState } from 'react';
import { LiabilityBreakdown } from './LiabilityBreakdown';
import { PaydownProgress } from './PaydownProgress';
import { ProjectionText } from './ProjectionText';
import getUserToken from '../../fetches/getUserToken';
import { Link } from 'react-router-dom';

export const Widgets = () => {
  const Spinwheel = window.Spinwheel;
  const [token, setToken] = useState(null);

  useEffect(()=>{
    async function getToken() {
     const accessToken = await getUserToken();
     setToken(accessToken);
    }
    !token && getToken();
  },[token])

  const openWidget = useCallback(
    (module, container, widgetConfig={}, spinwheelConfig={}) => {
      if(!token) return;
      const handler = Spinwheel.create({
        containerId: container,
        env: 'sandbox',
        onSettled: (response)=> {
          console.log(response, 'onSettle meta Data');
        },
        dropinConfig: {
          widget: true,
          module: module,
          token: token,
          ...widgetConfig
        },
        ...spinwheelConfig
      });
      handler.open();
      return handler;
    },
    [Spinwheel, token]
  );

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
      }}
    >
      <LiabilityBreakdown openWidget={openWidget} />

      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'space-between',
          alignItems: 'center',
          margin: '1rem 0',
          padding: '0.5rem 0',
          width: '90%',
          borderRadius: '0.5rem',
          background: '#ffffff',
        }}
      >
        <ProjectionText openWidget={openWidget} />
        <PaydownProgress openWidget={openWidget} />
      </div>
      <Link
        to="/projectionGraph"
        style={{
          textDecoration: 'none',
          padding: '1rem 2rem',
          border: '1px solid navy',
          borderRadius: '0.5rem',
          background: '#ffffff',
        }}
      >
        Projection Graph
      </Link>
    </div>
  );
};
