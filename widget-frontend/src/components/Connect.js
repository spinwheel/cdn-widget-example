import React, { useCallback, useEffect } from 'react';
import { useHistory } from 'react-router';
import getExtUserToken from '../fetches/getExtUserToken';

export const Connect = () => {
  const history = useHistory();
  const Spinwheel = window.Spinwheel;

  const openConnectDropin = useCallback(
    token => {
      const handler = Spinwheel.create({
        containerId: 'dropin-container',
        env: 'sandbox',
        onSuccess: response => {
          localStorage.setItem('accessToken', response.metadata.token);
          history.push(`/widgets`);
        },
        dropinConfig: {
          module: 'loan-servicers-login',
          token: token,
          header: 0,
        },
      });
      handler.open();
    },
    [Spinwheel, history]
  );

  useEffect(() => {
    const fetchTokenAndOpenDim = async () => {
      const token = await getExtUserToken()
      openConnectDropin(token);
    };
    fetchTokenAndOpenDim();
  }, [openConnectDropin]);

  return (
    <div style={{ height: '100%', width: '100%' }} id="dropin-container" />
  );
};
