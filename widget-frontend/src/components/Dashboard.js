import React from 'react';
import { useHistory } from 'react-router';
import {
  AppBar,
  Button,
  Container,
  makeStyles,
  Toolbar,
  Typography,
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: 'wrap',
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(6, 4, 0),
  },
  button: {
    marginTop: theme.spacing(5),
    padding: '8px 36px',
    fontSize: 16,
    fontWeight: 700,
  },
}));

export const Dashboard = () => {
  const classes = useStyles();
  const history = useHistory();

  const handleTryItOut = () => {
    history.push('/connect');
  };
  return (
    <>
      <AppBar
        position="static"
        color="default"
        elevation={0}
        className={classes.appBar}
      >
        <Toolbar className={classes.toolbar}>
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            XXXXXX Finance
          </Typography>
          <Button
            href="#"
            color="primary"
            variant="outlined"
            className={classes.link}
          >
            Login
          </Button>
        </Toolbar>
      </AppBar>
      <Container
        maxWidth="xs"
        align="center"
        component="main"
        className={classes.heroContent}
      >
        <svg
          width="83"
          height="94"
          viewBox="0 0 83 94"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <ellipse
            cx="39.1673"
            cy="38.4462"
            rx="31.1043"
            ry="29.5092"
            fill="#3F51B5"
          />
          <g filter="url(#filter0_d)">
            <rect
              x="4"
              y="45.766"
              width="47.8527"
              height="63.8037"
              transform="rotate(-73.0139 4 45.766)"
              fill="#3F51B5"
            />
          </g>
          <circle
            cx="41.5537"
            cy="32.8651"
            r="5.88037"
            fill="#FF9900"
            stroke="#FF9900"
          />
          <path
            d="M42.2986 34.1479L51.9322 65.5629L53.5273 69.5507L55.1224 72.7408L57.5144 75.9312L59.1095 77.5263L62.3001 80.7165L63.0974 83.1092L66.2875 91.8821"
            stroke="#FF9900"
          />
          <path d="M62.2969 80.7156L69.7797 89.5957" stroke="#FF9900" />
          <path d="M62.6572 92.322L62.0657 80.7247" stroke="#FF9900" />
          <defs>
            <filter
              id="filter0_d"
              x="0"
              y="0.000854492"
              width="83"
              height="75.4048"
              filterUnits="userSpaceOnUse"
              colorInterpolationFilters="sRGB"
            >
              <feFlood floodOpacity="0" result="BackgroundImageFix" />
              <feColorMatrix
                in="SourceAlpha"
                type="matrix"
                values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
              />
              <feOffset dy="7" />
              <feGaussianBlur stdDeviation="2" />
              <feColorMatrix
                type="matrix"
                values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.4 0"
              />
              <feBlend
                mode="normal"
                in2="BackgroundImageFix"
                result="effect1_dropShadow"
              />
              <feBlend
                mode="normal"
                in="SourceGraphic"
                in2="effect1_dropShadow"
                result="shape"
              />
            </filter>
          </defs>
        </svg>
        <Typography
          component="h1"
          variant="h4"
          align="center"
          color="textPrimary"
          gutterBottom
        >
          Easily manage student debt
        </Typography>
        <Typography
          variant="body1"
          align="center"
          color="textSecondary"
          component="p"
        >
          In a few minutes you can directly link to your student loan servicer
          account. Once linked, you can contribute to your loans with Round Ups
          and automatically apply rewards points you&apos;ve earned. With XXXXXX
          Finance it just got even easier to track contributions, monitor your
          progress, and get out of student debt faster.
        </Typography>
        <Button
          variant="contained"
          size="large"
          color="primary"
          className={classes.button}
          onClick={handleTryItOut}
        >
          Try It Out
        </Button>
      </Container>
    </>
  );
};
