import React, { useCallback, useEffect, useRef, useState } from 'react';
import getUserToken from '../fetches/getUserToken';

const initExtraPayment = 50

export const ProjectionGraph = () => {
  const Spinwheel = window.Spinwheel;
  const [extraPayment, setExtraPayment] = useState(initExtraPayment);
  const [token, setToken] = useState(null);
  const ref = useRef(null)


  const openWidget = useCallback(
    (widgetConfig={}) => {
      if (!token) return;
      const handler = Spinwheel.create({
        containerId: 'widget-container-i',
        env: 'sandbox',
        dropinConfig: {
          widget: true,
          module: 'projection-graph',
          token: token,
          ...widgetConfig,
        },
      });
      handler.open();
      return handler
    },
    [Spinwheel, token]
  );

    useEffect(() => {
      async function getToken() {
        const accessToken = await getUserToken();
        setToken(accessToken);
      }
      !token && getToken();
    }, [token]);

  useEffect(() => {
    const openWidgetFn = async () => {
      const handler = openWidget({
        extraMonthlyPayment: initExtraPayment,
      });
    ref.current = handler
    };
    if(token){
      openWidgetFn();
    }
  }, [openWidget, token]);

  useEffect(() => {
    if(!!ref.current) {
      ref.current.refresh({ extraMonthlyPayment: extraPayment })
    }
  }, [extraPayment])

  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
      }}
    >
      <div
        style={{
          width: '90%',
          margin: '1rem 0',
          padding: '1rem',
          height: '425px',
          overflow: 'auto',
          background: '#ffffff',
          borderRadius: '0.75rem',
        }}
        id="widget-container-i"
      />
      <div
        style={{
          width: '50%',
          margin: '1rem 0',
          padding: '0.5rem 1rem',
          display: 'flex',
          background: '#ffffff',
          borderRadius: '0.75rem',
          flexDirection: 'row',
          justifyContent: 'space-between',
        }}
      >
        <div
          onClick={() => setExtraPayment(extraPmt => extraPmt - 50)}
          style={{ fontSize: '2.5rem', fontWeight: '700' }}
        >
          -
        </div>
        <div
          style={{
            fontSize: '1.5rem',
            fontWeight: '700',
            lineHeight: '3.5rem',
          }}
        >
          {extraPayment}
        </div>
        <div
          onClick={() => setExtraPayment(extraPmt => extraPmt + 50)}
          style={{ fontSize: '2.5rem', fontWeight: '700' }}
        >
          +
        </div>
      </div>
    </div>
  );
};
