import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Connect } from './components/Connect';
import { Dashboard } from './components/Dashboard';
import { ProjectionGraph } from './components/ProjectionGraph';
import { Widgets } from './components/Widgets/Widgets';

export const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Dashboard} />
        <Route exact path="/connect" component={Connect} />
        <Route exact path="/widgets" component={Widgets} />
        <Route exact path="/projectionGraph" component={ProjectionGraph} />
      </Switch>
    </Router>
  );
};
