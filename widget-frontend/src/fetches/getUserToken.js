import axios from 'axios';
import jwt from 'jsonwebtoken';

const getUserToken = async () => {
  const accessToken = localStorage.getItem('accessToken');
  const decoded = jwt.decode(accessToken);
  const userId = decoded.matchedUsers
    ? decoded.matchedUsers[0].userId
    : decoded.userId;

  const resp = await axios({
    method: 'get',
    url: `http://localhost:8081/userId/${userId}`,
  });
  return resp.data;
};

export default getUserToken;
