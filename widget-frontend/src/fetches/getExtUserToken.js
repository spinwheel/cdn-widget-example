import axios from 'axios';

// WE'VE ADDED THIS DEFAULT VALUE FOR EXTERNAL USER ID, SO OUR APP DOES NOT CREATE NEW USERS
// EACH TIME WE RENDER THE CONNECT DROPIN. IN YOUR REAL APP YOU WOULD USE WHATEVER IDENTIFIER
// YOU COMMONLY USE FOR YOUR USERS INTERNALLY

const getExtUserToken = async (
  extUserId = '158a3456-5eda-429c-8759-b20f9493e102'
) => {
  const resp = await axios({
    method: 'get',
    url: `http://localhost:8081/extUserId/${extUserId}`,
  });
  return resp.data;
};

export default getExtUserToken;
