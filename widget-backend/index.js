const { default: axios } = require('axios');
const express = require('express');
const cors = require('cors');

const app = express();

const PORT = 8081;

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(cors());
app.options('*', cors());

// THIS IS A DUMMY SECRET KEY, IF YOU ATTEMPT TO USE THIS REPO WITHOUT REPLACING
// IT WITH YOUR OWN SANDBOX KEY, THE BELOW REQUESTS WILL FAIL WITH A 401 ERROR
const yourSecretKey = 'b141149e-bdec-49c2-854d-65db69ea8bf1';

const getToken = async (params) => {
  try {
    const resp = await axios({
      method: 'post',
      url: 'https://sandbox-swsl-api.spinwheel.io/v1/dim/token',
      data: params,
      headers: {
        Authorization: `Bearer ${yourSecretKey}`,
      },
    });
    return resp.data.data.token;
  } catch (error) {
    console.log(error);
  }
};

app.get('/extUserId/:id', (req, res) => {
  const { id } = req.params;
  getToken({extUserId: id})
    .then(token => {
      res.send(token);
    })
    .catch(e => {
      res.send(e);
    });
});

app.get('/userId/:id', (req, res) => {
  const { id } = req.params;
  getToken({userId: id})
    .then(token => {
      res.send(token);
    })
    .catch(e => {
      res.send(e);
    });
});

app.listen(PORT, () =>
  console.log(
    `Server started on port ${PORT}. ${
      yourSecretKey === 'b141149e-bdec-49c2-854d-65db69ea8bf1'
        ? '\nWARNING: You have not replaced the dummy key with your secret key in the backend index.js file. This application will not function properly until you do so.'
        : ''
    }`
  )
);
