# About Spinwheel CDN Widget Example

This application serves a model for how one could stand up an application that utilizes Spinwheel's Widgets via CDN. Some shortcuts necessarily have been taken for the sake of expediency, like the fact that there is no authentication between back and front end, the extUserId is hard coded in the Connect component, and styling...

# Getting Started
## Clone

To begin, open a terminal window and clone this repository onto your local machine using the command:

```
git clone git@bitbucket.org:spinwheel/cdn-widget-example.git
```

Once cloned, cd in the directory like so:

```
cd cdn-widget-example
```
## Booting the Backend Server

Navigate into the folder for the backend directory using the command:

```
cd widget-backend
```

Then, to install the node modules run the command:
```
yarn
```

IMPORTANT: On line 17 of the index.js file within the widget-backend folder, you must replace the dummy secret key with your own secret key. If you do not your token requests will fail with a 401 error.


Finally, start the backend server with the command:
```
yarn start
```

If done correctly, you'll see a console log in the terminal that reads: 'Server started on port 8081.'

## Booting the Frontend Application

Open a second terminal window (on Mac the shortcut is command + 'T') then navigate to the frontend directory with the command:

```
cd ../widget-frontend
```

In that window, install the node modules for the front end with the command:
```
yarn
```

Finally, start the frontend with the command:
```
yarn start
```

If you've followed instructions, your browser should automatically load the application and you should see the standard React success message in the terminal informing you that the application is live on port 3000.

# Using the Application

To use the application, click on the 'Try It Out' on the homepage that will send you to the page that loads the Connect DIM. From there, to use generic test credentials, after clicking through to any of the loan servicers enter 'testuser' as the username, and 'testpassword' as the password. Otherwise, if you happen to have a set of valid credentials, you can certainly use those to log in proceed through the application as you would expect. Once successfully connected, you'll be redirected to a page that renders three widgets with the information of the user that just logged in with the Connect DIM.